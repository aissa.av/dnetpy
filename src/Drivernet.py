import networkx as nx
import pandas as pd
import os,sys
import operator
from tqdm import tqdm
from networkx.algorithms import bipartite

class Data_Preprocessing:
    def load_inf_graph(file):
        inf_graph={}
        with open("../"+input_directory+"/"+file+".txt") as ifile:
            for line in ifile.readlines():
                line=line.strip().split("\t")
                if line[0] not in inf_graph:
                    inf_graph[line[0]]=[line[1]]
                else:
                    inf_graph[line[0]].append(line[1])

                if line[1] not in inf_graph:
                    inf_graph[line[1]]=[line[0]]
                else:
                    inf_graph[line[1]].append(line[0])

        return inf_graph

    def load_mutations_matrix(file):
        mutation_data=pd.read_csv("../"+input_directory+"/"+file+".csv",sep="\t")
        patients_mut=[pat for pat in mutation_data["Genes"]]
        patients_vs_mutations={}
        gene_vs_patients={}
        all_mutations=set()
        i=0
        for pat in patients:
            if pat in patients_mut:
                patient_row = mutation_data.loc[mutation_data['Genes'] == pat]
                idx=list(patient_row.index)[0]
                mutations_for_spec_patient=[cols for cols in patient_row if str(patient_row[cols][idx]) == "1"]
                for g in mutations_for_spec_patient:
                    all_mutations.add(g)
                    if g not in gene_vs_patients:
                        gene_vs_patients[g]=set()
                        gene_vs_patients[g].add(pat)
                    else:
                        gene_vs_patients[g].add(pat)
                patients_vs_mutations[pat]=mutations_for_spec_patient
        return all_mutations,patients_vs_mutations,gene_vs_patients


    def load_outliers_matrix(file):
        outliers_data=pd.read_csv("../"+input_directory+"/"+file+".csv")
        outliers_data=outliers_data.set_index(outliers_data["Genes"])
        patients_in_outliers_data=[col for col in outliers_data.index]
        outliers_data=outliers_data.transpose()
        all_outliers=set()
        all_outliers_=set()
        outliers_vs_patients={}
        gene_vs_patients_outliers={}

        for pat in patients:
            gene_vs_patients_outliers[pat]=[]
            if pat in patients_in_outliers_data:
                outliers_for_spec_patient=outliers_data.index[outliers_data[pat] == True].tolist()
                outliers_vs_patients[pat]=outliers_for_spec_patient
                for g in outliers_for_spec_patient:
                    all_outliers_.add(g)
                    if g not in gene_vs_patients_outliers:
                        gene_vs_patients_outliers[g]=[pat]
                    else:
                        gene_vs_patients_outliers[g].append([pat])


        outliers_vs_patients_={}
        i=0
        for pat in outliers_vs_patients:
            outliers_vs_patients_[pat]=[]
            for outlier in outliers_vs_patients[pat]:
                new_name=outlier+str("_")+pat
                all_outliers.add(new_name)
                outliers_vs_patients_[pat].append(new_name)
        return all_outliers,outliers_vs_patients,gene_vs_patients_outliers


class Graph:
    def construct_bipartite_graph(patient_mutations,patient_outliers):

        G = nx.Graph()
        G.add_nodes_from(mutated_genes, bipartite=0)
        G.add_nodes_from(outlier_genes, bipartite=1)
        pbar = tqdm(range(len(patients)))
        k=0
        for pat in patients:
            pbar.update(1)
            if pat in patient_mutations and pat in patient_outliers:

                for outlier in patient_outliers[pat]:
                    if outlier in inf_graph:
                        for mutation in patient_mutations[pat]:
                            if mutation in inf_graph:

                                if mutation in inf_graph[outlier]:
                                    outlier_=outlier+str("_")+pat
                                    G.add_edges_from([(mutation,outlier_ )])


        bipartite1_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==1}
        bipartite0_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==0}

        #deleting nodes with 0 degree
        for node in bipartite1_nodes:
            if int(G.degree(node))==0:
                G.remove_node(node)
        for node in bipartite0_nodes:
            if int(G.degree(node))==0:
                G.remove_node(node)
        print("Graph successfully generated with a size of: ",len(G.nodes())," nodes, and ",len(G.edges())," edges")


        return G


    def get_non_null_degrees(G):
        degrees = [(val,node) for (node, val) in G.degree()]
        null_degree=[]
        non_null_degree=[]
        for d in degrees:
            if d[0]==0:

                null_degree.append(d)
            else:
                #print(d)
                non_null_degree.append(d)

        mutations=[out[1] for out in non_null_degree if len(out[1].split("_"))==1]

        return mutations


    def DriverNet(G):
        bipartite1_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==1}
        bipartite0_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==0}

        methods_=["Dynamic"]

        for method in methods_:

            output="../out/BRCA_JCC_Dnet_"+method+".txt"
            with open (output,"w") as output_unweighted:
                mutations_degress=Graph.get_non_null_degrees(G)
                #print(len(mutations_degress))
                drivers=[]
                i=0
                bipartite1_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==1}

                while (len(bipartite1_nodes)!=0):
                    bipartite0_mutations= {n for n, d in G.nodes(data=True) if d['bipartite']==0}
                    i+=1
                    all_mutation_degrees={}
                    for mutated_gene in bipartite0_mutations:
                        mutated_gene_degree=G.degree(mutated_gene)
                        all_mutation_degrees[mutated_gene]=mutated_gene_degree

                    #sorted_mutation_list = sorted(all_mutation_degrees.items(), key=operator.itemgetter(1) ,reverse=True)
                    #sorted_list=[v[0] for v in sorted(all_mutation_degrees.iteritems(), key=lambda(k, v): (-v, k))]
                    sorted_mutation_list= [k for k, v in sorted(all_mutation_degrees.items(), key=lambda kv: (-kv[1], kv[0]))]

                    all_mutation_alpha_sorted=[]
                    for tuples in sorted_mutation_list:
                        if tuples[1] == sorted_mutation_list[0][1]:
                            all_mutation_alpha_sorted.append(tuples[0])
                        else:
                            break

                    all_mutation_alpha_sorted=sorted(all_mutation_alpha_sorted)

                    if method == "Dynamic":

                        ##Get the first gene -mutated- to be removed
                        node_to_remove=sorted_mutation_list[0]

                        drivers.append(node_to_remove)
                        outliers_to_delete= list(G.neighbors(node_to_remove))
                        for outlier_to_rmv in outliers_to_delete:
                            G.remove_node(outlier_to_rmv)
                        G.remove_node(node_to_remove)

                        bipartite1_nodes = {n for n, d in G.nodes(data=True) if d['bipartite']==1}

                        output_unweighted.write(node_to_remove)
                        output_unweighted.write(str("\n"))

                    else:
                        for gene in sorted_mutation_list:
                            output_unweighted.write(gene)
                            output_unweighted.write(str("\n"))
                        break

                output_unweighted.close()











def main():
    print("3 - Construct Bipartite Graph")
    global patients,mutated_genes,outlier_genes,inf_graph
    global input_directory
    # count the arguments
    arguments = len(sys.argv) - 1
    if arguments < 4:
        print("________________________________________________________________________________________")
        print('Please run the code using the following command line and Arguments:  ')
        print("python construct_bipartite_graph.py [Input Directory] [Patient Ids] [Influence Matrix] [Mutations Matrix] [Outliers Matrix]")
        print("________________________________________________________________________________________\n\n")
        sys.exit("")


    input_directory=sys.argv[1]
    patients_ids=sys.argv[2]
    influence_matrix=sys.argv[3]
    mutations_matrix=sys.argv[4]
    outliers_matrix=sys.argv[5]


    with open("../"+input_directory+"/"+patients_ids+".txt") as ifile:
        patients=[pat.strip() for pat in ifile.readlines()]


    #load data
    inf_graph=Data_Preprocessing.load_inf_graph(influence_matrix)
    mutated_genes,patient_mutations,mutation_patients=Data_Preprocessing.load_mutations_matrix(mutations_matrix)

    outlier_genes,patient_outliers,outlier_patients=Data_Preprocessing.load_outliers_matrix(outliers_matrix)
    G=Graph.construct_bipartite_graph(patient_mutations,patient_outliers)
    nx.write_gml(G, "../out/bipartite_graph.gml")
    Graph.DriverNet(G)








    print("*************************************************")
    print("*                Successfuly Done               *")
    print("*************************************************")


if __name__ == "__main__":
    main()
